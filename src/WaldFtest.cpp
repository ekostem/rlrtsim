/*
 * =====================================================================================
 *
 *       Filename:  WaldFtest.cpp
 *
 *    Description:  Wald's Variance Component Testing
 *                  y = X*Beta + Z_1 b_1 + Z_2 b_2 + e
 *                  We assume we know the variance of b_1 and test the presence of b_2
 *
 *        Version:  1.0
 *        Created:  04/25/2011 13:07:23
 *       Revision:  none
 *       Compiler:  icpc
 *
 *         Author:  EMRAH KOSTEM (EK), kostem@gmail.com
 *        Company:  www.emrahkostem.com
 *
 * =====================================================================================
 */


#include "../include/WaldFtest.h"


WaldTest::WaldTest(string phenotypeFile, string covarianceFile, string regionKinFile, string genomicKinFile, double varGenomic, bool verbose):isVerbose(verbose), m_varGenomic(varGenomic)
{
    m_Ym = m_Xm = m_regionKm = m_genomicKm = NULL;
    
    int tmp;
    string errorMSG;
    
    verboseMessage("Reading phenotypeFile");
    readFile2Matrix(phenotypeFile, m_Ym, m_nInd, tmp, errorMSG);
    if ( errorMSG.size() ) {
        verboseMessage( errorMSG );
        abort();
    }
    if (tmp > 1) {
        verboseMessage("Multiple pheotypes");
        abort();
    }

    verboseMessage("Reading covarianceFile");
    readFile2Matrix(covarianceFile, m_Xm, m_nXrow, m_nXcol, errorMSG);
    if ( errorMSG.size() ) {
        verboseMessage( errorMSG );
        abort();
    }
    if ( m_nXrow != m_nInd) {
        verboseMessage("Number individuals do not match between Y and X");
        abort();
    }

    verboseMessage("Reading region KinshipFile");
    readFile2Matrix(regionKinFile, m_regionKm, m_nregionK, tmp, errorMSG);
    if ( errorMSG.size() ) {
        verboseMessage( errorMSG );
        abort();
    }
    if ( m_nregionK != tmp ) {
        verboseMessage( "Region Kinship is not symmetric");
        abort();
    }

    verboseMessage("Reading genomic KinshipFile");
    readFile2Matrix(genomicKinFile, m_genomicKm, m_ngenomicK, tmp, errorMSG);
    if ( errorMSG.size() ) {
        verboseMessage( errorMSG );
        abort();
    }
    if ( m_ngenomicK != tmp ) {
        verboseMessage( "Genomic Kinship is not symmetric");
        abort();
    }

    if ( m_varGenomic <= 0.0 ) {
        verboseMessage("Genomic variance <=0");
        abort();
    }
}

WaldTest::~WaldTest()
{
    if ( m_Ym) delete [] m_Ym;
    if ( m_Xm) delete [] m_Xm;
    if ( m_regionKm) delete [] m_regionKm;
    if ( m_genomicKm) delete [] m_genomicKm;
}

void WaldTest::verboseMessage( string message )
{
    if ( isVerbose ) {
        cerr << message << endl;
    }
}

double WaldTest::statistic(void)
{
    /*
     * Compute the sqrt of the kinships, these are the Z matrices.
     */
    double *genomicZ = new double[m_nInd*m_nInd];
    double *regionZ = new double[m_nInd*m_nInd];

    string error = "";
    double statistic = 0.0;

    getSqrt(m_genomicKm, m_ngenomicK, genomicZ, error);
    if (error.size()) {
        verboseMessage( error );
    }

    getSqrt(m_regionKm, m_nregionK, regionZ, error);
    if (error.size()) {
        verboseMessage(error);
    }

	
    /*
     * Generate the concatenated matrices
     */
    double *X_alt = new double[ m_nInd*(m_nXcol + 2*m_nInd)];
    double *X_null = new double[ m_nInd*(m_nXcol + m_nInd)];
    
    concat( m_Xm, m_nXrow, m_nXcol, m_genomicKm, m_nInd, m_nInd, X_null, error);
    if (error.size()) {
        verboseMessage(error);
    }

    concat( X_null, m_nInd, (m_nXcol+m_nInd), m_regionKm, m_nInd, m_nInd, X_alt, error);
    if (error.size()) {
        verboseMessage(error);
    }

    /*
     * Generate the projection matrices, use generalized inverse
     */

    double *P_null = new double[m_nInd*m_nInd];
    double *P_alt = new double[m_nInd*m_nInd];

    getProjectionGinv( X_null, m_nInd, m_nXcol+m_nInd, P_null, error);
    if (error.size()) {
        verboseMessage(error);
    }

    getProjectionGinv( X_alt, m_nInd, m_nXcol+2*m_nInd, P_alt, error);
    if (error.size()) {
        verboseMessage(error);
    }

    /*
     *  Now some math to calculate the statistic
     *  First SSE(X,Z_1) = Y'(I - P_null)Y
     *  Second SSE(X, Z_1, Z_2) = Y'(I - P_alt)Y
     */
    int inc = 1;
    double alpha=1.0, beta=0.0;
    double YtY = ddot( &m_nInd, m_Ym, &inc, m_Ym, &inc); // Y' I Y
    
    
    
    double *PnY = new double[m_nInd];
    dgemv("N", &m_nInd, &m_nInd, &alpha, P_null, &m_nInd, m_Ym, &inc, &beta, PnY, &inc); // P_null Y
    double YtPnY = ddot(&m_nInd, m_Ym, &inc, PnY, &inc); // Y' P_null Y

    double *PaY = new double[m_nInd];
    dgemv("N",  &m_nInd, &m_nInd, &alpha, P_alt, &m_nInd, m_Ym, &inc, &beta, m_Ym, &inc); // P_alt Y
    double YtPaY = ddot(&m_nInd, m_Ym, &inc, PaY, &inc); // Y' P_alt Y

    m_df1 = trace( P_alt, m_nInd, error) - trace( P_null, m_nInd, error );
    m_df2 = m_nInd - trace( P_alt, m_nInd, error);

    statistic = ( YtPaY - YtPnY )*m_df2 / (  (YtY - YtPaY )*m_df1 ); 

    /*
     * Memory handling
     */
    if ( genomicZ ) delete [] genomicZ;
    if ( regionZ ) delete [] regionZ;
    if ( X_null ) delete [] X_null;
    if ( X_alt ) delete [] X_alt;
    if ( P_null ) delete [] P_null;
    if ( P_alt ) delete [] P_alt;

    return statistic;

}












