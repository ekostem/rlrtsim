// =====================================================================================
// 
//       Filename:  RLRTsim.cpp
// 
//    Description:  
// 
//        Version:  1.0
//        Created:  03/13/2011 17:18:20
//       Revision:  none
//       Compiler:  icpc
// 
//         Author:  EMRAH KOSTEM (EK), kostem@gmail.com
//        Company:  www.emrahkostem.com
//      Copyright:  Copyright (c) 2011, EMRAH KOSTEM
// =====================================================================================

#include "../include/RLRTsim.h"
#include "../include/InputFile.h"
#include "../include/RV.h"

RLRTsim::RLRTsim(string FixedEffectFilename, string KinshipFilename, int nGrid, int seed, bool verbose)
: m_nGrid(nGrid), seed(seed), isVerbose(verbose)
{
    sampleMultiplier = 10000;
    m_Km = NULL;
	m_Pm = NULL;
	m_Xm = NULL;
	m_Zm = NULL;
	m_ZspmValues = NULL;
	m_ZspRow = NULL; 
	m_ZspCol = NULL;
	gridLambda = NULL;
	gridOnes = NULL;
	m_PmEigenValues = NULL;
	gridEigenvalues = NULL;
	gridEigenvaluesDen = NULL;
	gridEigenvaluesDenInv = NULL;
	gridEigenvaluesLogTerm = NULL;
	logTerm = NULL;

	rvGenerator = new RV( seed );
    
    Fn_N = new double[m_nGrid];
    Fn_D = new double[m_nGrid];
    Fn_temp = new double[m_nGrid];


	string errorMSG = "";

	verboseMessage( "Reading the covariate matrix" );
    readFile2Matrix(FixedEffectFilename, m_Xm, m_nXrow, m_nXcol, errorMSG);
    if ( errorMSG.size() )
	{
        verboseMessage("Error reading: " + FixedEffectFilename );
        verboseMessage( errorMSG );
		abort();
	}
	m_nInd = m_nXrow;
	
	verboseMessage( "Reading the kinship matrix" );
	int temp1, temp2;
    readFile2Matrix(KinshipFilename, m_Km, temp1, temp2, errorMSG);
    if ( errorMSG.size() )
	{
        verboseMessage("Error reading: " + KinshipFilename);
        verboseMessage( errorMSG );
		abort();
	}
    

	if ( temp1 != temp2 )
	{
		verboseMessage( "Kinship matrix is not square" );
		abort();
	}
	else
	{
		m_nK = temp1;
	}

	if (m_nXrow != m_nInd || m_nXrow != m_nK)
	{
		verboseMessage( "Input matrix dimentions do not agree" );
		verboseMessage( "numInds: ", m_nInd);
		verboseMessage( "Xrow: ", m_nXrow);
		verboseMessage( "Kcol: ", m_nK);
		abort();
	}

	massageKinship(); // add to diagonal
	
	setm_Pm(); //Compute the matrix K^1/2 Z'(I - X(X'X)^-1X')ZK^1/2 into m_Pm
	
	setGrid(); // Set the grid where the algorithm searches for the root.	
}


RLRTsim::RLRTsim(string FixedEffectFilename, string KinshipFilename, string ZFilename, int nGrid, int seed, bool verbose)
: m_nGrid(nGrid), seed(seed), isVerbose(verbose)
{
    sampleMultiplier = 10000;
	m_Km = NULL;
	m_Pm = NULL;
	m_Xm = NULL;
	m_Zm = NULL;
	m_ZspmValues = NULL;
	m_ZspRow = NULL; 
	m_ZspCol = NULL;
	gridLambda = NULL;
	gridOnes = NULL;
	m_PmEigenValues = NULL;
	gridEigenvalues = NULL;
	gridEigenvaluesDen = NULL;
	gridEigenvaluesDenInv = NULL;
	gridEigenvaluesLogTerm = NULL;
	logTerm = NULL;
	
	rvGenerator = new RV( seed );
    Fn_N = new double[m_nGrid];
    Fn_D = new double[m_nGrid];
    Fn_temp = new double[m_nGrid];


	string errorMSG = "";

	verboseMessage( "Reading the covariate matrix" );
    readFile2Matrix(FixedEffectFilename, m_Xm, m_nXrow, m_nXcol, errorMSG);
    if ( errorMSG.size() )
	{
        verboseMessage("Error reading: " + FixedEffectFilename );
        verboseMessage( errorMSG );
		abort();
	}
	m_nInd = m_nXrow;
	
	verboseMessage( "Reading the Z matrix" );
    readFile2Matrix(ZFilename, m_Zm, m_nZrow, m_nZcol, errorMSG);
    if ( errorMSG.size() )
	{
        verboseMessage("Error reading: " + ZFilename );
        verboseMessage( errorMSG );
		abort();
	}


	sparseZ(); // Generate sparse Z
	
	verboseMessage( "Reading the kinship matrix" );
	int temp1, temp2;
    readFile2Matrix(KinshipFilename, m_Km, temp1, temp2, errorMSG);
    if ( errorMSG.size() )
	{
        verboseMessage("Error reading: " + KinshipFilename);
        verboseMessage( errorMSG );
		abort();
	}

	if ( temp1 != temp2 )
	{
		verboseMessage( "Kinship matrix is not square" );
		abort();
	}
	else
	{
		m_nK = temp1;
	}
	
	if (m_nZcol != m_nK)
	{
		verboseMessage( "Input matrix dimensions do not agree" );
		abort();
	}

	massageKinship(); // add to diagonal
	
	setm_Pm(); //Compute the matrix K^1/2 Z'(I - X(X'X)^-1X')ZK^1/2 into m_Pm
	
	setGrid(); // Set the grid where the algorithm searches for the root.	
}





RLRTsim::~RLRTsim()
{
	if (m_Km) delete [] m_Km;
	if (m_Pm) delete [] m_Pm;
	if (m_Xm) delete [] m_Xm;
	if (m_Zm) delete [] m_Zm;
	if (m_ZspmValues) delete [] m_ZspmValues;
	if (m_ZspRow) delete [] m_ZspRow; 
	if (m_ZspCol) delete [] m_ZspCol;
	if (gridOnes) delete [] gridOnes;
	if (gridLambda) delete [] gridLambda;
	if (m_PmEigenValues) delete [] m_PmEigenValues;
	if (gridEigenvalues) delete [] gridEigenvalues;
	if (gridEigenvaluesDen) delete [] gridEigenvaluesDen;
	if (gridEigenvaluesDenInv) delete [] gridEigenvaluesDenInv;
	if (gridEigenvaluesLogTerm) delete [] gridEigenvaluesLogTerm;
	if (logTerm) delete [] logTerm;
	if (rvGenerator) delete rvGenerator;
    if (Fn_N) delete [] Fn_N;
    if (Fn_D) delete [] Fn_D;
    if (Fn_temp) delete [] Fn_temp;
}


double RLRTsim::Fn( double *samples )
{
	int incx = 1;

	int remK = m_nInd - m_nXcol - m_nPmEigenValues;
	double n_p_term = 0.0;
	if ( remK > 0 )
	{
		n_p_term = dasum(&remK, samples + m_nPmEigenValues, &incx);
	}

	for (int i = 0; i < m_nGrid; i++)
	{
		Fn_N[i] = ddot(&m_nPmEigenValues, gridEigenvalues + i*m_nPmEigenValues, &incx, samples, &incx);
		Fn_D[i] = ddot(&m_nPmEigenValues, gridEigenvaluesDenInv + i*m_nPmEigenValues, &incx, samples, &incx);
		if (remK > 0)
		{
			Fn_D[i] += n_p_term;
		}
	}
	
	vdDiv(m_nGrid, Fn_N, Fn_D, Fn_temp);
	for (int i=0; i < m_nGrid; i++)
	{
		Fn_temp[i] += 1.0;
	}
	//vdLog10(m_nGrid, temp1, temp1);
	vdLn(m_nGrid, Fn_temp, Fn_temp);
	double A = (double)m_nInd - (double)m_nXcol;
	daxpy(&m_nGrid, &A, Fn_temp, &incx, Fn_temp, &incx);
		
	vdSub(m_nGrid, Fn_temp, logTerm, Fn_temp);
	
	double sup = 0.0;
    for (int i=0; i < m_nGrid; i++)
    {
        if ( Fn_temp[i] > sup )
        {
            sup = Fn_temp[i];
        }
    }

    return sup;
    //sort(Fn_temp, Fn_temp+m_nGrid, greater<double>());


	//return Fn_temp[0];
	
//	(nInd - 1.0)*log10(1.0 + N/D) - logterm;
	
}


void RLRTsim::sample(double *stats, unsigned long num)
{
	double sampleStatistic;
	verboseMessage( "sample: Sampling..." );
	if (!stats)
	{
		stats = new double[ num ];
	}
	double *samples = new double[m_nInd];
	for (unsigned long i=0; i<num; i++)
	{
		sampleChiSq(samples, m_nInd);
		stats[i] = Fn(samples);
	}
	verboseMessage( "sample: Sampled." );
}

double RLRTsim::pvalue(string storeFilename, int num, double query) 
{

    verboseMessage("Computing pvalue, sampling...");

    ofstream *fp = NULL;
   	if ( storeFilename.size() )
    {
        fp = new ofstream( storeFilename.c_str() );
    }

	double *samples = new double[ m_nInd -  m_nXcol ];
    double sampleStatistic;
    unsigned long greaterCount = 0;

    for (int turn = 0; turn < num; turn++) {
        verboseMessage("turn: ", turn);
        for (int i = 0; i < sampleMultiplier; i++) {
            
            sampleChiSq(samples,  m_nInd -  m_nXcol ); // sample n-p iid normal variates
            sampleStatistic = Fn(samples);
            if ( sampleStatistic < 0 ) 
            {
                sampleStatistic = 0.0;
            }
            if (fp) {
               	*fp << sampleStatistic << endl;
            }
            if ( sampleStatistic >= query ) {
                greaterCount++;
            }
        }
    } 
    if (fp) {
        delete fp;
    }
    delete [] samples;

    double pvalue = (double)greaterCount/(double)(sampleMultiplier);
    pvalue = pvalue / (double)num;
    verboseMessage("Pvalue: ", pvalue);
    return pvalue;
}


void RLRTsim::getEigenvalues(const double *Smatrix, int N, double *eigenvalues)
{
	int n = N, n2 = N*N, incx = 1, il, iu, m, lda = N, ldz = N, info, lwork, liwork;
	double abstol, vl, vu;
	int iwkopt;
	int* iwork = NULL;
	double wkopt;
	double* work = NULL;
	/* Local arrays */
	int* isuppz = new int[N*N];
	double* z = new double[N*N];
	double* W = new double[N*N];
	double* temp = new double[N*N];
	double *matrix = new double[N*N];
	
	dcopy(&n2, Smatrix, &incx, matrix, &incx);
	
	il = 1;
	iu = N;
	/* Query and allocate the optimal workspace */
	lwork = -1;
	liwork = -1;
	dsyevr( "N", "A", "U", &n, matrix, &lda, &vl, &vu, &il, &iu,
		   &abstol, &m, eigenvalues, z, &ldz, isuppz, &wkopt, &lwork, &iwkopt, &liwork,
		   &info );
	lwork = (int)wkopt;
	work = new double[lwork];
	liwork = iwkopt;
	iwork = new int[ liwork];
	/* Solve eigenproblem */
	dsyevr( "N", "A", "U", &n, matrix, &lda, &vl, &vu, &il, &iu,
		   &abstol, &m, eigenvalues, z, &ldz, isuppz, work, &lwork, iwork, &liwork,
		   &info );
	
	for (int i = 0; i < N; i++)
	{
		if ( eigenvalues[i] < 0.0 )
		{
			eigenvalues[i] = 1e-10;
		}
	}
	
	
	delete [] isuppz;
	delete [] z;
	delete [] W;
	delete [] temp;
	delete [] matrix;


}

void RLRTsim::sampleChiSq(double *samples, int num)
{
	rvGenerator->rNormal(num, 0, 1, samples);
	vdSqr(num, samples, samples);
}


void RLRTsim::verboseMessage( string message )
{
	if (isVerbose && message.size())
		cerr << message << endl;
}

void RLRTsim::verboseMessage( string message, int num)
{
	if (isVerbose)
		cerr << message << num << endl;
}

void RLRTsim::verboseMessage( string message, double num)
{
	if (isVerbose)
		cerr << message << num << endl;
}



void RLRTsim::storeSamples(double *stats, unsigned long num, string filename)
{
	
	ofstream fp(filename.c_str());
	for (unsigned long i=0; i<num; i++)
	{
		fp << stats[i] << endl;
	}
	fp.close();
}


void RLRTsim::setm_Pm(void)
{
	if ( !m_Pm )
	{
		m_Pm = new double[m_nK*m_nK]; // Alocate the matrix
	}
	string errorMSG = "";
	verboseMessage("setm_Pm: Computing I - X (X'X)^-1 X' "); 
	/*************************
	 * Compute the Projection Matrix
	 * I - X (X'X)^-1 X'
	 *************************/
	int incx = 1;
	double minone = -1.0;
	int n2 = m_nInd*m_nInd;
	double *projX = new double[ n2 ];
	getProjection(m_Xm, m_nXrow, m_nXcol, projX, errorMSG);
	verboseMessage(errorMSG);
	dscal(&n2, &minone, projX, &incx);
	for (int i=0; i < m_nInd; i++)
	{
		projX[i*m_nInd +i] += 1.0;
	}
	/****************************/
	
	
	/***************************************
	 * Compute the sqrt matrix of K
	 ***************************************/
	verboseMessage( "setm_Pm: Computing the Ksqrt matrix" );
	verboseMessage( "m_nK: ", m_nK);
    double alpha = 1.0, beta = 0.0;
	double *Ksqrt = new double[m_nK*m_nK];
	getSqrt(m_Km, m_nK, Ksqrt, errorMSG);
    verboseMessage(errorMSG);
    verboseMessage("setm_Pm: Ksqrt computed" );



	
	double *ZKsqrt = NULL;
//	if ( m_Zm )
//	{
//		verboseMessage( "setm_Pm: Z K^1/2" );
//		// Z K^1/2
//		dgemm("N", "N", &m_nZrow, &m_nK, &m_nZcol, &alpha, m_Zm, &m_nZrow, Ksqrt, &m_nK, &beta, ZKsqrt, &m_nZrow);
//	}
    bool delZKsqrt = true;
	if ( m_ZspmValues)
	{
		ZKsqrt = new double[m_nZrow*m_nZcol];
		verboseMessage( "setm_Pm: Z K^1/2" );
		char matdescra[6];
		matdescra[0] = 'G';
		matdescra[1] = 'L';
		matdescra[2] = 'N';
		matdescra[3] = 'C';
		
		mkl_dcoomm("N", &m_nZrow, &m_nZcol, &m_nZcol, &alpha, matdescra, m_ZspmValues, m_ZspRow, m_ZspCol, &m_nZnnz, Ksqrt, &m_nZcol, &beta, ZKsqrt, &m_nZcol);
	}
	else
	{
        delZKsqrt = false;
		ZKsqrt = Ksqrt;
	}
	
	verboseMessage( "setm_Pm: (I - X (X'X)^-1 X')ZK^1/2 " );
	// (I - X (X'X)^-1 X')ZK^1/2 ( m_nInd x m_nK )
	double *auxMatrix = new double[m_nInd*m_nK];
	dgemm("N", "N", &m_nInd, &m_nK, &m_nInd, &alpha, projX, &m_nInd, ZKsqrt, &m_nInd, &beta, auxMatrix, &m_nInd);
	
	verboseMessage( "setm_Pm: K^1/2 Z' (I - X (X'X)^-1 X')ZK^1/2 " );
	// m_Pm = (ZK^1/2)'(I - X (X'X)^-1 X')ZK^1/2
	dgemm("T", "N", &m_nK, &m_nK, &m_nInd, &alpha, ZKsqrt, &m_nInd, auxMatrix, &m_nInd, &beta, m_Pm, &m_nK);
	/*****************************************/
	
	
	verboseMessage( "setm_Pm: Cleaning...." );
	if ( projX ) delete [] projX;
	if ( Ksqrt ) delete [] Ksqrt;
	if ( ZKsqrt && delZKsqrt) delete [] ZKsqrt;
	if ( auxMatrix ) delete [] auxMatrix;
}


void RLRTsim::setGrid(void)
{
	/*************************
	 * Generate the Grid
	 *************************/
	
	gridLambda = new double[ m_nGrid ];
	gridOnes = new double[ m_nK ];
	for (int i=0; i < m_nK; i++)
	{
		gridOnes[i] = 1.0;
	}
	
	for (int i=0; i < m_nGrid; i++)
	{
		gridLambda[i] = -12.0 + 24.0*(double)i/(double)m_nGrid;
	}
	
	vdExp(m_nGrid,  gridLambda, gridLambda);
	
	/*************************
	 * Compute the Eigenvalues
	 *************************/
	
	if ( !m_Pm )
	{
		verboseMessage("Error: Projection matrix is not ready!");
		abort();
	}
	verboseMessage("Computing m_Pm eigenvalues");
	m_PmEigenValues = new double[m_nK];
	getEigenvalues(m_Pm, m_nK, m_PmEigenValues);
    sort( m_PmEigenValues, m_PmEigenValues+m_nK, greater<double>() );
    

    m_nPmEigenValues = 1; // get the number of useful eigenvalues
    while( m_nPmEigenValues < m_nK ) 
    {
        if ( (m_PmEigenValues[ m_nPmEigenValues-1 ] / m_PmEigenValues[ m_nPmEigenValues ] ) > 10.0 ) 
        {
            break;
        }
        m_nPmEigenValues++;
    }

    verboseMessage("setGrid m_nPmEigenValues: ", m_nPmEigenValues);

	/***************************
	 * Cache the grid points.
	 ***************************/
	int n = m_nGrid * m_nPmEigenValues;
	int incx = 1;
	gridEigenvalues = new double[ n ];
	gridEigenvaluesDen = new double[ n ];
	gridEigenvaluesDenInv = new double[ n ];
	gridEigenvaluesLogTerm = new double[ n ];
	logTerm = new double[m_nGrid];
	for (int i=0; i < m_nGrid; i++)
	{
		dcopy(&m_nPmEigenValues, m_PmEigenValues, &incx, gridEigenvalues+i*m_nPmEigenValues, &incx);
        dscal(&m_nPmEigenValues, gridLambda+i, gridEigenvalues+i*m_nPmEigenValues, &incx);
	}
    dcopy(&n, gridEigenvalues, &incx, gridEigenvaluesDen, &incx);

	for (int i=0; i<n; i++)
	{
		gridEigenvaluesDen[i] += 1.0;
	}
	vdDiv(n, gridEigenvalues, gridEigenvaluesDen, gridEigenvalues); // \lambda \mu_l / (1 + \lambda \mu_l)
	vdInv(n, gridEigenvaluesDen, gridEigenvaluesDenInv); // 1 / (1 + \lambda \mu_l)
	//vdLog10(n, gridEigenvaluesDen, gridEigenvaluesLogTerm); // log( 1 + \lambda \mu_l )
	vdLn(n, gridEigenvaluesDen, gridEigenvaluesLogTerm); // log( 1 + \lambda \mu_l )
    for (int i=0; i < m_nGrid; i++)
	{
		logTerm[i] = ddot(&m_nPmEigenValues, gridOnes, &incx, gridEigenvaluesLogTerm + i*m_nPmEigenValues, &incx);
	}
}


void RLRTsim::massageKinship(void)
{
	verboseMessage( "Adding to kinship diagonal" );
	for (int i=0; i < m_nK; i++)
	{
		m_Km[i*m_nK + i] += 1e-5;
	}
}


void RLRTsim::sparseZ(void)
{
	verboseMessage("Sparsing Z matrix.");
	m_nZnnz = m_nInd; // We know how many 1's there are in the Z matrix one per individual
	m_ZspmValues = new double[m_nZnnz];
	m_ZspRow = new int[m_nZnnz];
	m_ZspCol = new int[m_nZnnz];
	int index = 0;
	for (int row=0; row < m_nZrow; row++)
	{
		for (int col=0; col < m_nZcol; col++)
		{
			if ( m_Zm[m_nZrow*col + row] > 0.0)
			{
				m_ZspmValues[index] = m_Zm[m_nZcol*col + row];
				m_ZspRow[index] = row;
				m_ZspCol[index] = col;
				index++;
			}
		}
	}
}









