/*
 * =====================================================================================
 *
 *       Filename:  main_test.cpp
 *
 *    Description:  Test functions for MatFun
 *
 *        Version:  1.0
 *        Created:  04/24/2011 15:07:50
 *       Revision:  none
 *       Compiler:  icpc
 *
 *         Author:  EMRAH KOSTEM (EK), kostem@gmail.com
 *        Company:  www.emrahkostem.com
 *
 * =====================================================================================
 */


#include "../include/MathFun.h"
#include <string>
#include <iostream>
using namespace std;

int main()
{
	int N = 4;
	
    double A[16] = {
        1.8665176,  1.194562,  0.4834126,  0.5248570,
        1.1945616,  4.745940,  1.6576447, -2.1133588,
        0.4834126,  1.657645,  1.5634867, -0.5561778,
        0.5248570, -2.113359, -0.5561778,  1.6752137
	};
	

    double SA[8] = {
        -0.6740102,  1.4413094,  0.8992485, -1.269278,
        -1.0955518, -0.5189429, -1.3299991,  1.262381
    };


    double B[16];
    double SB[16];
    string error;
    printMatrix(A, N, N);
    
    cout << endl;
    cout << endl;

    getSqrt(A, N, B, error);
    cout << error << endl;
    printMatrix(B, N, N);

    //getProjection(A, N, N, B, error);
    //printMatrix(B,N,N);



    printMatrix(SA, 4, 2);
    getProjectionGinv(SA, 4, 2, SB, error);
    printMatrix(SB, 4, 4);


    double C[32];

    concat(A,4,4,B,4,4,C,error);
    printMatrix(C,4,8);

    getProjectionGinv(C, 4, 8, B, error);
    //cout << error << endl;
    printMatrix(B,4,4);
    cout << "Done!" << endl;
    
    return 0;
}

