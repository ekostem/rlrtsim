//
//  MathFun.cpp
//  admmaXcode
//
//  Created by Emrah Kostem on 4/22/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//

#include "../include/MathFun.h"

/* template<class T>
 * void printMatrix(T *M, int nrow, int ncol)
 * {
 *     for (int i=0; i < nrow; i++) {
 *         for (int j=0; j < ncol; j++) {
 *             cout << M[j*nrow + i] << " ";
 *         }
 *         cout << endl;
 *     }
 *     cout << endl;
 * }
 * 
 */

double betaIn(double x, double p, double q, string &errorMSG)
{
	double acu = ACCURACY;
	double ai;
	double cx;
	bool indx;
	int ns;
	double pp;
	double psq;
	double qq;
	double rx;
	double temp;
	double term;
	double value;
	double xx;
    
	value = x;
	errorMSG = "";
	//
	//  Check the input arguments.
	//
	if ( p <= 0.0 || q <= 0.0 )
	{
		errorMSG = "Incomplete Beta: a or b is <=0";
		return value;
	}
	
	if ( x < 0.0 || 1.0 < x )
	{
		errorMSG = "Incomplete Beta: x is < 0.0 or > 1.0";
		return value;
	}
	//
	//  Special cases.
	//
	if ( fabs(x-0.0) < 1e-20 || fabs(x - 1.0) < 1e-20 )
	{
		return value;
	}
	
	//
	// Compute the BETA(a,b)
	//
	double beta = lgamma(p) + lgamma(q) - lgamma(p+q);
	//
	//  Change tail if necessary and determine S.
	//
	psq = p + q;
	cx = 1.0 - x;
	
	if ( p < psq * x )
	{
		xx = cx;
		cx = x;
		pp = q;
		qq = p;
		indx = true;
	}
	else
	{
		xx = x;
		pp = p;
		qq = q;
		indx = false;
	}
	
	term = 1.0;
	ai = 1.0;
	value = 1.0;
	ns = ( int ) ( qq + cx * psq );
	//
	//  Use the Soper reduction formula.
	//
	rx = xx / cx;
	temp = qq - ai;
	if ( ns == 0 )
	{
		rx = xx;
	}
	
	for ( ; ; )
	{
		term = term * temp * rx / ( pp + ai );
		value = value + term;;
		temp = fabs( term );
		
		if ( temp <= acu && temp <= acu * value )
		{
			value = value * exp ( pp * log ( xx ) 
								 + ( qq - 1.0 ) * log ( cx ) - beta ) / pp;
			
			if ( indx )
			{
				value = 1.0 - value;
			}
			break;
		}
		
		ai = ai + 1.0;
		ns = ns - 1;
		
		if ( 0 <= ns )
		{
			temp = qq - ai;
			if ( ns == 0 )
			{
				rx = xx;
			}
		}
		else
		{
			temp = psq;
			psq = psq + 1.0;
		}
	}
	
	return value;
	
}


double cdfF(double x, int df1, int df2, string &errorMSG)
{
	return betaIn( df1*x/(df1*x+df2), 0.5*df1, 0.5*df2, errorMSG);	
}


void getProjection(const double *X, int nrow, int ncol, double *P, string &errorMSG)
{
	errorMSG = "";
	if (!X)
	{
		errorMSG = "getProjection: X is NULL!";
		return;
	}
	if (!P)
	{
		errorMSG = "getProjection: P is NULL!";
		return;
	}
	
	int *ipiv = new int[ncol];
	double *XtX = new double[ ncol*ncol ];
	double *Xt = new double[ncol*nrow];
	int info;
	double zero = 0.0;
	double one = 1.0;
	
	dgemm("T", "N", &ncol, &ncol, &nrow, &one, X, &nrow, X, &nrow, &zero, XtX, &ncol); // X'X
	
	// Get the LU of X'X
	dgetrf(&ncol, &ncol, XtX, &ncol, ipiv, &info);
	if (info != 0)
	{
		errorMSG = "getProjection: Problem in LU decomposition in getProjection";
	}
	
	mkl_domatcopy('C', 'T', nrow, ncol, one, X, nrow, Xt, ncol);


	//Get the (X'X)^-1 X'
	dgetrs("N", &ncol, &nrow, XtX, &ncol, ipiv, Xt, &ncol, &info);
	if (info != 0)
	{
		errorMSG = "getProjection: Problem in Solve in getProjection";
	}
	
	// Now Xt = (X'X)^-1 X', get X (X'X)^-1 X'
	dgemm("N", "N", &nrow, &nrow, &ncol, &one, X, &nrow, Xt, &ncol, &zero, P, &nrow);
	
	delete [] ipiv;
	delete [] XtX;
	delete [] Xt;
}




void getProjectionGinv(const double *X, int nrow, int ncol, double *P, string &errorMSG)
{
	errorMSG = "";
	if (!X)
	{
		errorMSG = "getProjection: X is NULL!";
		return;
	}
	if (!P)
	{
		errorMSG = "getProjection: P is NULL!";
		return;
	}
	
	double *XtX = new double[ncol*ncol];
    int maxd = max(ncol, nrow);

	double *Xt = new double[ncol*nrow];
	double zero = 0.0;
	double one = 1.0;
	
	dgemm("T", "N", &ncol, &ncol, &nrow, &one, X, &nrow, X, &nrow, &zero, XtX, &ncol); // X'X
   
    mkl_dimatcopy('C', 'N', ncol, nrow, zero, Xt, ncol, ncol);
    
    mkl_domatcopy('C', 'T', nrow, ncol, one, X, nrow, Xt, ncol); // X'

	int info, lwork, rank;
	double rcond = -1.0;
	double wkopt;
	double *work = NULL;
   
	int *iwork = new int[11*maxd*maxd];
	double *s = new double[maxd];




	lwork = -1;
	dgelsd( &ncol, &ncol, &nrow, XtX, &ncol, Xt, &ncol, s, &rcond, &rank, &wkopt, &lwork, iwork, &info );


    lwork = (int)wkopt;
    work = new double[lwork];
    
	/* Solve the equations A*X = B */
	dgelsd( &ncol, &ncol, &nrow, XtX, &ncol, Xt, &ncol, s, &rcond, &rank, work, &lwork, iwork, &info );
	/* Check for convergence */
	if( info > 0 ) 
	{
		errorMSG = "getProjectionGinv: The algorithm computing SVD failed to converge";
	}
	
	// Now Xt = (X'X)^-1 X', get X (X'X)^-1 X'
	dgemm("N", "N", &nrow, &nrow, &ncol, &one, X, &nrow, Xt, &ncol, &zero, P, &nrow);
	
	delete [] XtX;
	delete [] Xt;
    delete [] iwork;
    delete [] work;
	delete [] s;
}

int trace(const double *X, int n, string &errorMSG)
{
	errorMSG = "";
	double tr = 0.0;
	for (int i=0; i<n; i++)
	{
		tr += X[i*n+i];
	}
	return (int)tr;
}


void getSqrt(const double *X, int N, double *SQRT, string &errorMSG)
{
	errorMSG = "";
	
    int numEigenvalues, lda = N, ldz = N, il, iu, info;
    double abstol, vl, vu;
    
    double *work = NULL;
    int lwork;
    double wkopt;

    int *iwork = NULL, liwork, iwkopt;

    int *isuppz = new int[2*N]; 
    double *eigenValues = new double[N];
    double *eigenVectors = new double[N*N];
    
    
    /* Make a copy of the X Matrix */
    double *Xcpy = new double[N*N];
	mkl_domatcopy('C', 'N', N, N, 1.0, X, N, Xcpy, N); 
	/* Query and allocate the optimal workspace */
	lwork = -1;
	liwork = -1;
	dsyevr( "V", // Jobz:   Compute Eigenvalues and EigenVectors 
            "A", // Range:  Compute all Eigenvalues
            "U", // Uplo:   Store the upper triangular part
            &N, Xcpy, &lda, 
            &vl, &vu, // Not referenced
            &il, &iu, // Not referenced
            &abstol, 
            &numEigenvalues, eigenValues, 
            eigenVectors, &ldz, 
            isuppz, 
            &wkopt, &lwork, &iwkopt, &liwork, &info );

	lwork = (int)wkopt;
	work = new double[lwork];
	liwork = iwkopt;
	iwork = new int[liwork];
	
    /* Solve eigenproblem */
    dsyevr( "V", // Jobz:   Compute Eigenvalues and EigenVectors 
            "A", // Range:  Compute all Eigenvalues
            "U", // Uplo:   Store the upper triangular part
            &N, Xcpy, &lda, 
            &vl, &vu, // Not referenced
            &il, &iu, // Not referenced
            &abstol, 
            &numEigenvalues, eigenValues, 
            eigenVectors, &ldz, 
            isuppz, 
            work, &lwork, iwork, &liwork, &info );

    /* Check for convergence */
    
    if ( info > 0  ) {
        errorMSG = "getSqrt: dsyevr failed to compute eigenvalues";
        return;
    }
    /* Clean negative eigenvalues */
    numEigenvalues = 0;
	for (int i=0; i < N; i++) {
		if ( eigenValues[i] < EIGTHRSHLD ) {
			eigenValues[i] = 0.0;
		}
        else {
            numEigenvalues++;
        }
	}
    /* SQRT eigenvalues */
	vdSqrt(N, eigenValues, eigenValues);
    
    double *tmpValues = new double[numEigenvalues];
    int *rows = new int[numEigenvalues];
    int *columns = new int[numEigenvalues];
    int pos = 0;
    for (int i=0; i < N; i++) {
        if ( eigenValues[i] > 0.0 ) {
            tmpValues[pos] = eigenValues[i];
            rows[pos] = i;
            columns[pos] = i;
            pos++;
        }
    }
    
    /* Transpose EigenVectors */
    //mkl_dimatcopy('C', 'T', N, N, 1.0, eigenVectors, N, N);
    
    /*
     * Zero based indexing in mkl_dcoom accepts the second matrix as transposed
     */
    double *temp = new double[N*N];
    char matdesc[6];
    matdesc[0] = 'G';
    matdesc[1] = 'L';
    matdesc[2] = 'L';
    matdesc[3] = 'C';

	char transA = 'N';
    double alpha = 1.0, beta = 0.0;
    mkl_dcoomm( &transA, &N, &N, &N, &alpha, matdesc, 
                tmpValues, rows, columns, &numEigenvalues, 
                eigenVectors, &N, 
                &beta, temp, &N);
	
    
    
    dgemm("N", "T", &N, &N, &N, &alpha, eigenVectors, &N, temp, &N, &beta, SQRT, &N);
	

    delete [] work;
    delete [] iwork;
	delete [] isuppz;
    delete [] eigenValues;
    delete [] eigenVectors;
    delete [] Xcpy;
    delete [] tmpValues;
    delete [] rows;
    delete [] columns;
    delete [] temp;
	
}



void concat(const double *X1, int nrow1, int ncol1, const double *X2, int nrow2, int ncol2, double *C, string &errorMSG)
{
    errorMSG = "";

    if ( nrow1 != nrow2 ) {
        errorMSG = "concat: Matrix dimensions do not agree";
        return;
    }
    int size1 = nrow1*ncol1;
    int size2 = nrow2*ncol2;
    int inc = 1;

    dcopy(&size1, X1, &inc, C, &inc);
    dcopy(&size2, X2, &inc, &C[size1], &inc);
}


void readFile2Matrix(string filename, double *&matrix, int &nrow, int &ncol, string &errorMSG)
{
    errorMSG = "";

	InputFile *File = NULL;
	if (matrix)
	{
		delete [] matrix;
	}
	
	try
	{
		File = new InputFile(filename);
		nrow = File->length();
		ncol = File->at(0)->size();
		matrix = new double[ nrow*ncol ];
		for (int i = 0; i < nrow; i++)
		{
			for (int j = 0; j < ncol; j++)
			{
                matrix[j*nrow+ i] = strtod( File->at(i)->at(j).c_str(), NULL);
            }       
		}
	}
	catch (InputFilexception& e)
	{
		errorMSG = "readFile2Matrix: " + (string)e.what();
	}
	if (File) 
	{
		delete File;
	}
}


    







