// =====================================================================================
// 
//       Filename:  main.cpp
// 
//    Description:  
// 
//        Version:  1.0
//        Created:  03/13/2011 17:20:51
//       Revision:  none
//       Compiler:  icpc
// 
//         Author:  EMRAH KOSTEM (EK), kostem@gmail.com
//        Company:  www.emrahkostem.com
//      Copyright:  Copyright (c) 2011, EMRAH KOSTEM
// =====================================================================================

#include "../include/MathFun.h"
#include "../include/RLRTsim.h"
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <string>
using namespace std;


void printHelp();
bool checkFilename(string, char);


void printHelp()
{
	cerr << "Usage: RLRTsim [-c covf] [-z zinf] [-k kinf] [-g #grid] [-n #samples] [-s seed] [-q stat] [-o statf] [-p pvalf] -v" << endl;
	cerr << "\tn->#individuals, q->#covariates (including intercept)" << endl;
	cerr << "\tRLRTsim returns the pvalue of the query statistic" << endl;
	cerr << "\tRequired Parameters:" << endl;
	cerr << "\t-c covf               : n x q, covariance file" << endl;
	cerr << "\t-k kinf               : n x n (or m x m with -z option), kinship file" << endl;
    cerr << "\t-q stat               : query statistic" << endl;
	cerr << "\tOptional Parameters:" << endl;
	cerr << "\t-z zinf               : n x m, Z file" << endl;
	cerr << "\t-g #grid              : grid size for RLRT simulation (default: 200)" << endl;
	cerr << "\t-n #samples           : number of sample statistic (x10^4) under the null distribution (default: 1, i.e. 10000)" << endl;
	cerr << "\t-s seed               : integer seed for random number generator (default: 333)" << endl;
	cerr << "\t-o statf              : stores sampled statistics to statf file" << endl;
	cerr << "\t-p pvalf              : writes pvalue to pvalf file" << endl;
	cerr << "\t-v                    : verbose mode" << endl;
}

bool checkFilename( string filename, char option )
{
	if ( filename == "" )
	{
		cerr << "Missing option [-" << option << "]" << endl;
		return true;
	}
	return false;
}


int main(int argc, char **argv)
{
	string covariateMatrixFilename = "";
	string kinshipMatrixFilename = "";
	string ZFilename = "";
	string sampledStatisticsStoreFilename = "";
	string pvalueStoreFilename = "";
	
	int gridSize = 200;
	unsigned long nullStatisticSampleSize = 1;
	int seed = 333;
	
	double queryStatistic = -1.0;

	bool verboseFlag = false;
	
	char c;
	while ( (c = getopt(argc, argv, "c:z:k:g:n:s:q:p:o:v")) != -1)
	{
		switch (c)
		{
			case 'c':
				covariateMatrixFilename = string( optarg );
				break;
			case 'z':
				ZFilename = string( optarg );
				break;			
			case 'k':
				kinshipMatrixFilename = string( optarg );
				break;
			case 'g':
				gridSize = atoi(optarg);
				break;
			case 'n':
				nullStatisticSampleSize *= atoi(optarg);
				break;
			case 's':
				seed = atoi(optarg);
				break;
			case 'q':
				queryStatistic = strtod(optarg, NULL);
				break;
			case 'o':
				sampledStatisticsStoreFilename = string(optarg);
				break;
			case 'p':
				pvalueStoreFilename = string(optarg);
				break;
			case 'v':
				verboseFlag = true;
				break;
			default:
				cerr << "Error: Unknown option: -" << c << endl;
				printHelp();
				abort();
		}
	}
	if ( argc < 5 )
	{
		printHelp();
		abort();
	}
	
	if ( checkFilename(covariateMatrixFilename, 'c') )
	{
		abort();
	}
	
	if ( !checkFilename(ZFilename, 'z') && checkFilename(kinshipMatrixFilename, 'k') )
	{
		abort();
	}
	
	if ( checkFilename(kinshipMatrixFilename, 'k') )
	{
		abort();
	}
	
	
	if ( queryStatistic < 0.0 )
	{
		cerr << "Error: query statistic [-q]" << endl;
		abort();
	}
	
	/*********************
	 * Allocations
	 *********************/
	RLRTsim *Sim = NULL;
	double pvalue = 1.0;
    if ( queryStatistic < 1e-10 )
    {
        cout << "Pvalue: " << pvalue << endl;
        if ( pvalueStoreFilename.size() )
        {
            ofstream fp( pvalueStoreFilename.c_str() );
            fp << pvalue << endl;
            fp.close();
        }
        return 0;
    }
	
	if ( ZFilename.size() )
	{
		Sim = new RLRTsim(covariateMatrixFilename, kinshipMatrixFilename, ZFilename, gridSize, seed, verboseFlag);
	}
	else
	{
		Sim = new RLRTsim(covariateMatrixFilename, kinshipMatrixFilename, gridSize, seed, verboseFlag);
	}
	
	pvalue = Sim->pvalue(sampledStatisticsStoreFilename, nullStatisticSampleSize, queryStatistic);
	cout << "Pvalue: " << pvalue << endl;
	if ( pvalueStoreFilename.size() )
	{
		ofstream fp( pvalueStoreFilename.c_str() );
		fp << pvalue << endl;
		fp.close();
	}
	
	
	
	/********************
	 * Cleanup the memory
	 ********************/
	if (Sim) delete Sim;
	
	
	
	return 0;	
}









