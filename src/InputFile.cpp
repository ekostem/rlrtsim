/*
 *  InputFile.cpp
 *  OFSS
 *
 *  Created by Emrah Kostem on 2/16/10.
 *  Copyright 2010 _EMRAH_KOSTEM_. All rights reserved.
 *
 */


#include "../include/InputFile.h"

InputFile::InputFile(const string filename, const char* delimiter, const char* skip)
{	
	if ( filename.size() == 0 )
	{
		throw InputFilexception("Empty Filename " + filename_);
	}
	
	filename_ = filename;
	ifstream fp (filename_.c_str());
	if ( ! fp.is_open())
	{
		throw InputFilexception("Error Opening " + filename_);
	}
	
	/*************************
	 Get the size of the file
	 *************************/
	fp.seekg(0, ios::end);
	std::streampos length = fp.tellg();
	fp.seekg(0, ios::beg);
	
	/*************************
	 Read the whole file into buffer
	 *************************/
	vector<char> buffer(length);
	fp.read(&buffer[0],length);
	fp.close();
	/*************************
	 Create string stream.
	 Get the stringbuffer from the stream and set the vector as its source.
	**************************/
	stringstream localStream;
	localStream.rdbuf()->pubsetbuf(&buffer[0],length);
	
	string line;
	string word;
	if (delimiter)
	{
		while ( getline(localStream, line) )
		{
			if (!skip || (skip && line.find(*skip) != 0) )
			{
				istringstream iss(line);
				vector <string>* parsed_line = new vector <string>;
				while(getline(iss, word, *delimiter  ))
				{
					parsed_line->push_back(word);
				}
				lines_.push_back(parsed_line);				
			}			
		}
		
	
	}
	else
	{
		while ( getline(localStream, line) )
		{
			if (!skip || (skip && line.find(*skip) != 0) )
			{
				istringstream iss(line);
				vector<string>* parsed_line = new vector<string>;
				copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter< vector<string> >(*parsed_line));
				lines_.push_back(parsed_line);				
			}
		}

	}
	file_begin = lines_.begin();
	file_end = lines_.end();
}

InputFile::~InputFile()
{
	// Clear the storage
	for (current_line = file_begin; current_line != file_end; ++current_line)
	{
		//Empty the vector<string> content
		(*current_line)->clear();
		// Delete the allocated pointer
		delete *current_line;
	}
	//Clear vector<Lines*>
	lines_.clear();
}

vector <string>* InputFile::operator[](int i) const
{
	if ( i < 0 || i > lines_.size() )
	{
		string what("Out of bounds");
		throw InputFilexception(what);
	}
	return lines_[i];
}

vector <string>* InputFile::at(int i) const
{
	return lines_[i];
}

void InputFile::print()
{
	cout << "File Content" << endl;
	for (current_line = file_begin; current_line != file_end; ++current_line)
	{
		line_begin = (*current_line)->begin();
		line_end = (*current_line)->end();
		for ( line_pos = line_begin; line_pos != line_end; ++line_pos)
		{
			cout << *line_pos << "\t" ;
		}
		cout << endl;
	}
}
