//
//  LMESolve.cpp
//  admmaXcode
//
//  Created by Emrah Kostem on 3/14/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//

#include "../include/LMESolve.h"
#include "../include/MKLconf.h"

LMESolve::LMESolve(const double *Y, const double *X, const double* Kr, const double *Kg, int nY, int nBeta, int numGridPoints)
:m_numIndividuals(nY), m_nBeta(nBeta), m_numGridPoints(numGridPoints)
{
	int nK = m_numIndividuals*m_numIndividuals;
	int nX = m_numIndividuals*m_nBeta;
	
	m_Y = new double[m_numIndividuals];
	m_X = new double[nX];
	m_Kr = new double[nK];
	m_Kg = new double[nK];
	m_Beta = new double[m_nBeta];
	m_grid = new double[m_numGridPoints];
	
	// Create the grid
	for (int i=0; i<m_numGridPoints; i++)
	{
		m_grid[i] = 0.0 + 1.0*(double)i/m_numGridPoints;
	}
	vdExp(m_numGridPoints, m_grid, m_grid);
	
	int incx=1;
	double zero=0.0;
	
	dcopy(&m_numIndividuals, Y, &incx, m_Y, &incx);
	dcopy(&nX, X, &incx, m_X, &incx);
	dcopy(&nK, Kr, &incx, m_Kr, &incx);
	dcopy(&nK, Kg, &incx, m_Kg, &incx);
	
	dscal(&m_nBeta, &zero, m_Beta, &incx);
}

LMESolve::~LMESolve()
{
	if ( m_Y ) delete [] m_Y;
	if ( m_X ) delete [] m_X;
	if ( m_Kr ) delete [] m_Kr;
	if ( m_Kg ) delete [] m_Kg;
	if ( m_Beta ) delete [] m_Beta;
	if ( m_grid ) delete [] m_grid;
	
}

double LMESolve::logLikelihood(double varR, double varG, double varE, bool isNull) const
{
	// L = constant - log(det(V)) - log(det(X' V.inv X)) - (y- Xbeta.hat)' V.inv (y - Xbeta.hat)
	
	double constant = -1.0*m_numIndividuals*log(2.0*M_PI);
	double one = 1.0, zero = 0.0;
	int incx = 1;
		
	int nV = m_numIndividuals*m_numIndividuals;
	int nY = m_numIndividuals;

	double *V = new double[nV];
	double *V_LU = new double[nV];
	double *XVX = new double[m_nBeta*m_nBeta];
	double *V_Eigenvalues = new double[m_numIndividuals];
	double *V_Eigenvectors = new double[nV];
	double *XBeta_hat = new double[nY];
	
	dscal(&nV, &zero, V, &incx );
	daxpy(&nV, &varG, m_Kg, &incx, V, &incx);
	if (!isNull)
	{
		daxpy(&nV, &varR, m_Kr, &incx, V, &incx);
	}
	for (int i=0; i<m_numIndividuals; i++)
	{
		V[i*m_numIndividuals + i] += varE;
	}
	// Here we have V
	// Get spectral decomposition
	eigen(V, m_numIndividuals, V_Eigenvalues, V_Eigenvectors);

	// Compute determinant of V
	double detV = 1.0;
	for (int i=0; i<m_numIndividuals; i++)
	{
		detV *= V_Eigenvalues[i];
	}

	// Compute X' V.inv X
	double *temp1 = new double[m_numIndividuals * m_nBeta];
	double *temp2 = new double[m_nBeta * m_nBeta];
	double *temp3 = new double[m_numIndividuals];
	double *temp4 = new double[m_nBeta];
	double *temp5 = new double[m_nBeta];
	double *diff = new double[m_numIndividuals];
	double *V_invDiff = new double[m_numIndividuals];
	
	
	solve(V, temp1, m_X, m_numIndividuals, m_nBeta); // V.inv * X
	dgemm("T", "N", &m_nBeta, &m_nBeta, &m_numIndividuals, &one, m_X, &m_numIndividuals, temp1, &m_numIndividuals, &zero, temp2, &m_nBeta ); // X' V.inv X	
	double detXVX = m_nBeta > 1 ? detLU(temp2, m_nBeta) : *temp2;
	
	solve(V, temp3, m_Y, m_numIndividuals, 1);
	int ione = 1;
	dgemm("T", "N", &m_nBeta, &ione, &m_numIndividuals, &one, m_X, &m_numIndividuals, temp3, &m_numIndividuals, &zero, temp4, &m_nBeta ); // X' V.inv Y
	
	solve(temp2, temp5, temp4, m_nBeta, 1); // beta hat
	dgemm("N", "N", &m_numIndividuals, &m_nBeta, &m_nBeta, &one, m_X, &m_numIndividuals, temp5, &m_numIndividuals, &zero, XBeta_hat, &m_numIndividuals ); // X Beta.hat
	
	vdSub(m_numIndividuals, m_Y, XBeta_hat, diff);
	solve(V, V_invDiff, diff, m_numIndividuals, 1);
	double yVy = ddot(&m_numIndividuals, diff, &incx, V_invDiff, &incx);
	
	
	
	delete [] V;
	delete [] V_LU;
	delete [] XVX;
	delete [] V_Eigenvalues;
	delete [] V_Eigenvectors;
	delete [] XBeta_hat;
	
	delete [] temp1;
	delete [] temp2;
	delete [] temp3;
	delete [] temp4;
	delete [] temp5;
	delete [] diff;
	delete [] V_invDiff;
	
	return (constant - log(detV) - log(detXVX) - yVy);
	
}









double LMESolve::gridSearch(bool isNull) const
{
	
	double REML = -10000.0;
	double val;
	int maxr, maxg, maxe;
	
	if ( isNull )
	{
		for (int g=0; g<m_numGridPoints; g++)
		{
			for (int e=0; e<m_numGridPoints; e++)
			{
				val = logLikelihood(0.0, *(m_grid+g), *(m_grid+e), isNull);
				if (val > REML)
				{
					REML = val;
					maxg = g;
					maxe = e;
				}
			}
		}
		cout << 0.0 << " " << *(m_grid + maxg) << " " << *(m_grid + maxe) << endl;
	}
	else
	{
		for (int r=0; r<m_numGridPoints; r++)
		{
			for (int g=0; g<m_numGridPoints; g++)
			{
				for (int e=0; e<m_numGridPoints; e++)
				{
					val = logLikelihood(*(m_grid+r), *(m_grid+g), *(m_grid+e), isNull);
					if (val > REML)
					{
						REML = val;
						maxr = r;
						maxg = g;
						maxe = e;
					}
				}
			}		
		}
		cout << *(m_grid + maxr) << " " << *(m_grid + maxg) << " " << *(m_grid + maxe) << endl;	
	}
	
	return REML;
}


void LMESolve::solve(const double* A, double *X, const double* B, int nn, int p) const
{
	int n = nn;
	int lda = n;
	int size, incx = 1, info;
	int nrhs = p;
	int* ipiv = new int[ n ];
	double *Acopy = new double[n*n];
	
	size = n*n;
	dcopy(&size, A, &incx, Acopy, &incx);
	size = n*p;
	dcopy(&size, B, &incx, X, &incx);
	
	dgetrf( &n, &n, Acopy, &lda, ipiv, &info ); // LU decomposition
	if (info)
	{
		cerr << "Problem with LU factorization in solve" << endl;
	}
		
	dgetrs( "N", &n, &nrhs, Acopy, &lda, ipiv, X, &lda, &info ); // Solve for the correlation term, this is infact Sigma^-1 %*% R_iT
	if (info)
	{
		cerr << "Problem with solver" << endl;
	}
		
	delete [] ipiv;
	delete [] Acopy;
}



void LMESolve::eigen(const double *Smatrix, int N, double *values, double *vectors) const
{
	int n = N, n2 = N*N, incx = 1, il, iu, m, lda = N, ldz = N, info, lwork, liwork;
	double abstol, vl, vu;
	int iwkopt;
	int* iwork = NULL;
	double wkopt;
	double* work = NULL;
	/* Local arrays */
	int* isuppz = new int[N*N];
	double* temp = new double[N*N];

	// make copy
	double *matrix = new double[N*N];
	dcopy(&n2, Smatrix, &incx, matrix, &incx);
	
	il = 1;
	iu = N;
	/* Query and allocate the optimal workspace */
	lwork = -1;
	liwork = -1;
	dsyevr( "V", "A", "U", &n, matrix, &lda, &vl, &vu, &il, &iu,
		   &abstol, &m, values, vectors, &ldz, isuppz, &wkopt, &lwork, &iwkopt, &liwork,
		   &info );
	lwork = (int)wkopt;
	work = new double[lwork];
	liwork = iwkopt;
	iwork = new int[ liwork];
	/* Solve eigenproblem */
	dsyevr( "V", "A", "U", &n, matrix, &lda, &vl, &vu, &il, &iu,
		   &abstol, &m, values, vectors, &ldz, isuppz, work, &lwork, iwork, &liwork,
		   &info );
	
	delete [] isuppz;
	delete [] temp;
	delete [] matrix;
}



double LMESolve::detCholesky(const double* matrix, int dim) const
{
	int n = dim;
	int n2 = n*n;
	int incx = 1;
	int lda = dim;
	int info;
	double detVal = 1.0;
	double *temp = new double[n2];
	
	dcopy(&n2, matrix, &incx, temp, &incx);
	
	
	dpotrf( "L", &n, temp, &lda, &info ); // Cholesky decomposition of the Sigma Tag SNPs
	if (info)
	{
		cerr << "Error in detCholesky: " << info << endl;
	}
	
	for (int i = 0; i<n; i++)
	{
		detVal *= temp[ i*dim + i];
	}
	detVal *= detVal;
	delete [] temp;
	return detVal;
}



double LMESolve::detLU( const double* Smatrix, int dim) const
{
	int m = dim;
	int n = dim;
	int n2 = n*n, incx=1;
	int lda = dim;
	int info;
	int* ipiv = new int[dim];
	double det = 1.0;
	
	double *matrix = new double[n2];
	dcopy(&n2, Smatrix, &incx, matrix, &incx);
	
	
	
	dgetrf( &m, &n, matrix, &lda, ipiv, &info );
	if (info)
	{
		cout << "Error in detLU: " << info << endl;
	}
	
	for (int i=0; i<n; i++)
	{
		if (ipiv[i] != i)
		{
			det = -det * matrix[i*n + i];
		}
		else 
		{
			det = det * matrix[i*n + i];
		}
	}
	delete [] matrix;
	return det;
}





void LMESolve::printMatrix(double* matrix, int nrow, int ncol) const
{
	cout.precision(3);
	
	for (int i=0; i<nrow; i++)
	{
		for (int j=0; j< ncol; j++)
		{
			cout << matrix[ j*nrow + i] << " ";			
		}
		cout << endl;
	}
	cout << endl;
}


void LMESolve::setY(double* newY)
{
	int incx = 1;
	dcopy(&m_numIndividuals, newY, &incx, m_Y, &incx);
}
