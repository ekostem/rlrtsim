//
//  RV.cpp
//  ADMMA
//
//  Created by Emrah Kostem on 3/13/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//

#include "../include/RV.h"


RV::RV(const int seed, const int brng)
{
	brng_ = brng;
	seed_ = seed;
	vslNewStream( &stream_, brng_, seed_ );
}

void RV::rNormal(int count, double mean, double std, double* output, int method)
{
	vdRngGaussian( method, stream_, count, output, mean, std );
}

void RV::rMVNormal(int count, int dimension, const double* mean, const double* VarCovMatrix, double* output, int method)
{
	int info, incx=1, n=dimension*dimension;
	double *tempVarCov = new double[n];
	dcopy(&n, VarCovMatrix, &incx, tempVarCov, &incx);
	dpotrf("U", &dimension, tempVarCov, &dimension , &info);
	vdRngGaussianMV( method, stream_, count, output, dimension, VSL_MATRIX_STORAGE_FULL, mean, tempVarCov );
	delete [] tempVarCov;
}

void RV::rUniform(int count, double left, double right, double* output, int method)
{
	vdRngUniform( method, stream_, count, output, left, right );
}
