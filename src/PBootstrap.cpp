//
//  PBootstrap.cpp
//  admmaXcode
//
//  Created by Emrah Kostem on 3/14/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//

#include "../include/PBootstrap.h"
#include "../include/InputFile.h"
#include "../include/RV.h"
#include "../include/LMESolve.h"

PBootstrap::PBootstrap(string XFilename, string infoFilename, string KrFilename, string KgFilename, int sampleSize, int seed) : m_numSamples(sampleSize), m_seed(seed)
{
	
	m_X = NULL;
	m_Beta = NULL;
	m_Kr = NULL;
	m_Kg = NULL;
	
	m_varR = 0.0;
	m_varG = 0.0;
	m_varE = 0.0;
	
	if (!readFile2Matrix(XFilename, m_X, m_numIndividuals, m_nBeta) )
	{
		cerr << "Error reading " << XFilename << endl;
	}
	else
	{
		m_Beta = new double[ m_nBeta ];
	}

	if ( !readFile2Matrix(KrFilename, m_Kr, m_nKr, m_nKr) )
	{
		cerr << "Error reading " << KrFilename << endl;
	}
	
	if ( !readFile2Matrix(KgFilename, m_Kg, m_nKg, m_nKg) )
	{
		cerr << "Error reading " << KgFilename << endl;
	}
	
	InputFile *File = NULL;
	try 
	{
		File = new InputFile(infoFilename);
		int len = File->length();
		string temp;
		for (int i=0; i<len; i++)
		{
			temp = File->at(i)->at(0);
			if ( temp == "Beta" )
			{
				for (int j=0; j < m_nBeta; j++)
				{
					m_Beta[j] = strtod( File->at(i)->at(j+1).c_str(), NULL);
				}
			}
			
			if ( temp == "varR" )
			{
				m_varR = strtod( File->at(i)->at(1).c_str(), NULL);
			}

			if ( temp == "varG" )
			{
				m_varG = strtod( File->at(i)->at(1).c_str(), NULL);
			}

			if ( temp == "varE" )
			{
				m_varE = strtod( File->at(i)->at(1).c_str(), NULL);
			}
		}
		
	}
	catch (InputFilexception& e)
	{
		cerr << e.what() << endl;
	}
	
//	cout << "Beta: ";
//	printMatrix(m_Beta, m_nBeta, 1);
//	cout << "varR: " << m_varR << endl;
//	cout << "varG: " << m_varG << endl;
//	cout << "varE: " << m_varE << endl;
	
	
	//scale the covariance matrices.
	int n = m_nKr*m_nKr;
	for (int i=0; i<n; i++)
	{
		m_Kr[i] = m_varR * m_Kr[i];
	}
	n = m_nKg*m_nKg;
	for (int i=0; i<n; i++)
	{
		m_Kg[i] = m_varG * m_Kg[i];
	}
	
	
	m_rvGenerator = new RV(m_seed);
	
	if (File)
	{
		delete File;
	}
}

PBootstrap::~PBootstrap()
{
	if (m_X) delete [] m_X;
	if (m_Beta) delete [] m_Beta;
	if (m_Kr) delete [] m_Kr;
	if (m_Kg) delete [] m_Kg;
	if (m_rvGenerator) delete m_rvGenerator;
}



void PBootstrap::printMatrix(double* matrix, int nrow, int ncol) const
{
	cout.precision(3);
	
	for (int i=0; i<nrow; i++)
	{
		for (int j=0; j< ncol; j++)
		{
			cout << matrix[ j*nrow + i] << " ";			
		}
		cout << endl;
	}
	cout << endl;
}


bool PBootstrap::readFile2Matrix(string filename, double *&matrix, int &n, int &m)
{
	bool ret = true;
	InputFile *File = NULL;
	if (matrix)
	{
		delete [] matrix;
	}
	try
	{
		File = new InputFile(filename);
		n = File->length();
		m = int(File->at(0)->size());
		matrix = new double[ n*m ];
		for (int j = 0; j < m; j++)
		{
			for (int i = 0; i < n; i++)
			{
				matrix[j*n + i] = strtod( File->at(i)->at(j).c_str(), NULL);
			}
		}
	}
	catch (InputFilexception& e)
	{
		cerr << e.what() << endl;
		ret = false;
	}
	if (File) 
	{
		delete File;
	}
	
	return ret;
}



bool PBootstrap::generateSamples(double *&samples, int &numSamples, int &numIndividuals) const
{
	if (!samples)
	{
		samples = new double[ m_numSamples * m_numIndividuals];
	}
	numSamples = m_numSamples;
	numIndividuals = m_numIndividuals;
	
	double one=1.0, zero=0.0;
	int incx = 1, n=m_numIndividuals*m_numSamples;
	double *error = new double[n];
	double *fixedEffects = new double[m_numIndividuals];
	double *randomEffectR = new double[n]; 
	double *randomEffectG = new double[n]; 
	double *rndMean = new double[m_numIndividuals];
	

	// condition the rndMean
	dscal(&m_numIndividuals, &zero, rndMean, &incx);
	// init. samples
	dscal(&n, &zero, samples, &incx);
	
	
	// Create the fixed effects vector.
	dgemv("N", &m_numIndividuals, &m_nBeta, &one, m_X, &m_numIndividuals, m_Beta, &incx, &zero, fixedEffects, &incx);

	for (int i=0; i<m_numSamples; i++)
	{
		dcopy(&m_numIndividuals, fixedEffects, &incx, samples+i*m_numIndividuals, &incx);
	}

	// Generate error
	m_rvGenerator->rNormal( m_numIndividuals*m_numSamples, 0.0, sqrt(m_varE), error );
	// Generage random effects
	m_rvGenerator->rMVNormal( m_numSamples, m_numIndividuals, rndMean, m_Kr, randomEffectR );
	m_rvGenerator->rMVNormal( m_numSamples, m_numIndividuals, rndMean, m_Kg, randomEffectG );
	
	vdAdd(n, samples, error, samples);
	vdAdd(n, samples, randomEffectR, samples);
	vdAdd(n, samples, randomEffectR, samples);

//	printMatrix(samples, m_numIndividuals, m_numSamples);
	
	delete [] error;
	delete [] rndMean;
	delete [] fixedEffects;
	delete [] randomEffectR;
	delete [] randomEffectG;
	return true;
}





void PBootstrap::testLMESolve() const
{
	
	double *samples = NULL, *Y = NULL;
	int numsamples, numind, incx = 1;
	
	
	Y = new double[m_numIndividuals];

	generateSamples(samples, numsamples, numind); 
	dcopy(&m_numIndividuals, samples, &incx, Y, &incx); 
	
	LMESolve *lme = new LMESolve(Y, m_X, m_Kr, m_Kg, m_numIndividuals, m_nBeta, 10);

//	Lval = lme->logLikelihood(10, 2, 1, true);
//	cout << "REML: " << Lval << endl;
//	
//	Lval = lme->logLikelihood(5, 1, 3, true);
//	cout << "REML: " << Lval << endl;
//
//	Lval = lme->logLikelihood(5, 5, 1, true);
//	cout << "REML: " << Lval << endl;
//
//	Lval = lme->logLikelihood(0, 0.26, 0.47, true);
//	cout << "REML: " << Lval << endl;

	
	double Lval_HA = lme->gridSearch(false);
	cout << "REML: " << Lval_HA << endl;
	
//	lme->setY(samples + numind);
	double Lval_H0 = lme->gridSearch(true);
	cout << "REML: " << Lval_H0 << endl;
	
	cout << Lval_HA - Lval_H0 << endl;
	
	
	delete [] samples;
	delete [] Y;
	delete lme;
}













