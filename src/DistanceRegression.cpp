//
//  DistanceRegression.cpp
//  admmaXcode
//
//  Created by Emrah Kostem on 4/12/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//

#include "../include/DistanceRegression.h"


DistanceRegression::DistanceRegression(double *Kinship, int nrowKin, double *X, int ncolX)
:m_nrow(nrowKin), m_ncol(ncolX)
{
	m_Km = new double[m_nrow*m_nrow];
	m_Xm = new double[m_nrow*m_ncol];
	m_Gm = new double[m_nrow*m_nrow];
	m_Hm = new double[m_nrow*m_nrow];
	m_Im = new double[m_nrow*m_nrow];
	
	int n = m_nrow*m_nrow;
	int inc = 1;
	//copy Kinship matrix to local
	dcopy(&n, Kinship, &inc, m_Km, &inc);

	//create the idendity matrix
	double zero = 0.0;
	dscal(&n, &zero, m_Im, &inc);
	for (int i = 0; i < m_nrow; i++)
	{
		m_Im[i*n + i] = 1.0;
	}	
	
	n = m_nrow*m_ncol;
	//copy predictor to local
	dcopy(&n, X, &inc, m_Xm, &inc);
	
	// Create the projection matrix to H
	getProjection(m_Xm, m_Hm, m_nrow, m_ncol);
	
	// Center the Kinship matrix to G
	centralize(m_Km, m_Gm, m_nrow);
}

DistanceRegression::~DistanceRegression()
{
	delete [] m_Km;
	delete [] m_Xm;
	delete [] m_Gm;
	delete [] m_Hm;
	delete [] m_Im;	
}


double DistanceRegression::computePseudoF(double *G, double *H, double *I, int n) const
{
	if (!G || !H || !I )
	{
		return 0.0;
	}
	
	double pseudoFvalue = 0.0;
	int n2 = n*n;
	double zero = 0.0;
	double one = 1.0;	
	double *HG = new double[n2];
	double *Ho = new double[n2];
	double *HoG = new double[n2];
	
	dgemm("N", "N", &n, &n, &n, &one, H, &n, G, &n, &zero, HG, &n); // HG
	
	vdSub(n2, I, H, Ho); // (I-H)
	dgemm("N", "N", &n, &n, &n, &one, Ho, &n, G, &n, &zero, HoG, &n); // (I-H)G
	
	pseudoFvalue = trace(HG, n) / trace(HoG, n);
	
	delete [] HG;
	delete [] Ho;
	delete [] HoG;
	return pseudoFvalue;
}



void DistanceRegression::samplePseudoF(double *G, double *H, double *I, int n, double *samples, int sampleSize)
{
	if (!G || !H || !I)
	{
		return;
	}
	
	if ( samples == NULL )
	{
		samples = new double[sampleSize];
	}
	
	int n2 = n*n;
	double *Gpermute = new double[n2];
	
	for (int i=0; i < sampleSize; i++)
	{
		permute(G, Gpermute, n, n, i);
		samples[i] = computePseudoF(Gpermute, H, I, n); 
	}
}


double DistanceRegression::getPvalue(double pseudoFvalue, double *samples, int sampleSize) const
{
	if (!samples)
	{
		return 0.0;
	}
	
	double Pvalue = 0.0;
	
	for (int i=0; i < sampleSize; i++)
	{
		if ( samples[i] < pseudoFvalue )
		{
			Pvalue += 1.0;
		}
	}
	Pvalue = Pvalue/(double)sampleSize;
	return Pvalue;
}

void DistanceRegression::getProjection(double *X, double *Y, int nrow, int ncol)
{
	if (!X)
	{
		return;
	}
	if ( Y == NULL)
	{
		Y = new double[ nrow*nrow ];
	}
	
	int *ipiv = new int[ncol];
	double *XtX = new double[ ncol*ncol ];
	double *Xt = new double[ncol*nrow];
	int info;
	double zero = 0.0;
	double one = 1.0;
	
	dgemm("T", "N", &ncol, &ncol, &nrow, &one, X, &nrow, X, &nrow, &zero, XtX, &ncol); // X'X
	
	// Get the LU of X'X
	dgetrf(&ncol, &ncol, XtX, &ncol, ipiv, &info);
	if (info != 0)
	{
		cerr << "Problem in LU decomposition in getProjection" << endl;
	}
	
	//Get the X'
	int inc = 1;
	int jump = nrow; 
	for (int i=0; i < ncol; i++)
	{
		dcopy(&ncol, X, &inc, &Xt[i], &jump); 
	}
	
	
	//Get the (X'X)^-1 X'
	dgetrs("N", &ncol, &nrow, XtX, &ncol, ipiv, Xt, &ncol, &info);
	if (info != 0)
	{
		cerr << "Problem in Solve in getProjection" << endl;
	}
	
	// Now Xt = (X'X)^-1 X', get X (X'X)^-1 X'
	dgemm("N", "N", &nrow, &nrow, &ncol, &one, X, &nrow, Xt, &ncol, &zero, Y, &nrow);
	
	delete [] ipiv;
	delete [] XtX;
	delete [] Xt;
}

void DistanceRegression::centralize(double *X, double *Y, int n)
{
	if (!X)
	{
		return;
	}
	
	if ( Y == NULL)
	{
		Y = new double[n*n];
	}
	int n2 = n*n;
	double scalar = -1.0/(double)n;
	double one = 1.0;
	double zero = 0.0;
	double *P = new double[n*n];
	double *temp = new double[n*n];
	for (int i=0; i < n2; i++)
	{
		P[i] = scalar;
	}
	
	for (int i=0; i<n; i++)
	{
		P[i*n+i] += 1.0;
	}
	
	dgemm("N", "N", &n, &n, &n, &one, P, &n, X, &n, &zero, temp, &n);
	dgemm("N", "N", &n, &n, &n, &one, temp, &n, P, &n, &zero, Y, &n);
	
	
	delete [] P;
	delete [] temp;
}

double DistanceRegression::trace(double *X, int n) const
{
	if (!X)
	{
		return 0.0;
	}
	double tr = 0.0;
	for (int i=0; i<n; i++)
	{
		tr += X[i*n + i];
	}
	return tr;
}

void DistanceRegression::permute(double *X, double *Y, int nrow, int ncol, int seed)
{
	// Permutes the rows of matrix X, stores in Y
	if (!X)
	{
		return;
	}
	if (!Y)
	{
		Y = new double[nrow*ncol];
	}

	RV *rand = new RV(seed);
	double *perm = new double[ nrow ];
	int *p = new int[ nrow ];
	rand->rUniform( nrow, 0, nrow*nrow, perm);
	
	for (int i = 0; i < nrow; i++) 
	{
		p[i] = i;
	}
	
	
	int j;
	for (int i = 0; i < nrow; i++) 
	{
		j = (int)perm[i] % (i + 1);
		p[i] = p[j];
		p[j] = i;
	}
	
	int swrow;
	for (int i = 0; i < nrow; i++)
	{
		swrow = p[i];
		dcopy(&ncol, &X[swrow], &nrow, &Y[i], &nrow);		
	}
	
	delete rand;
	delete [] p;
	delete [] perm;
	
}











