/*
 * =====================================================================================
 *
 *       Filename:  main_wald.cpp
 *
 *    Description:  Wald's variance component test, test utility
 *
 *        Version:  1.0
 *        Created:  04/25/2011 23:22:46
 *       Revision:  none
 *       Compiler:  icpc
 *
 *         Author:  EMRAH KOSTEM (EK), kostem@gmail.com
 *        Company:  www.emrahkostem.com
 *
 * =====================================================================================
 */


#include "../include/MathFun.h"
#include "../include/WaldFtest.h"

#include <iostream>
using namespace std;

int main(int argc, const char *argv[])
{
    
    WaldTest * WT = NULL;

    WT = new WaldTest("./Y.txt", "./X.txt", "./rK.txt", "gK.txt", 4.0, true);
	cout << WT->statistic();

	delete WT;

    return 0;
}

