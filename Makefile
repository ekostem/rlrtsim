OS = $(shell uname -s)
CXX = icpc -static -static-intel
CFLAGS = -O3 -Wall -vec-report1 -funroll-loops -ip
LIBDIR = -L$(MKLROOT)/lib/em64t 
MKLPATH = $(MKLROOT)/lib/em64t
INCLUDE = -I$(MKLROOT)/include

ifeq ($(OS),Darwin)
	CFLAGS += -mmacosx-version-min=10.6
	LIBS = -lmkl_solver_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread
	#LIBS = -lmkl_solver_lp64_sequential -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread

endif

ifeq ($(OS),Linux)
	#LIBS = -lmkl_solver_lp64 -Wl,--start-group -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -Wl,--end-group -liomp5 -lpthread
	LIBS = -lmkl_solver_lp64_sequential -Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -lpthread
endif

OBJECTS = MathFun.o RV.o DistanceRegression.o InputFile.o RLRTsim.o LMESolve.o PBootstrap.o WaldFtest.o main.o
TEST = MathFun.o RV.o DistanceRegression.o InputFile.o RLRTsim.o LMESolve.o PBootstrap.o WaldFtest.o main_test.o
WALD = MathFun.o RV.o DistanceRegression.o InputFile.o RLRTsim.o LMESolve.o PBootstrap.o WaldFtest.o main_wald.o



wald : $(WALD)
	$(CXX) $(CFLAGS) $(WALD) $(INCLUDE) $(LIBDIR) $(LIBS) -o wald
	ctags -R --c++-kinds=+cp --fields=+iamS --extra=+fq .

test : $(TEST)
	$(CXX) $(CFLAGS) $(TEST) $(INCLUDE) $(LIBDIR) $(LIBS) -o test
	ctags -R --c++-kinds=+cp --fields=+iamS --extra=+fq .

all : $(OBJECTS)
	$(CXX) $(CFLAGS) $(OBJECTS) $(INCLUDE) $(LIBDIR) $(LIBS) -o RLRTsim
	ctags -R --c++-kinds=+cp --fields=+iamS --extra=+fq .

main_wald.o : ./src/main_wald.cpp
	$(CXX) $(CFLAGS) -c ./src/main_wald.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

main_test.o : ./src/main_test.cpp
	$(CXX) $(CFLAGS) -c ./src/main_test.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

main.o : ./src/main.cpp
	$(CXX) $(CFLAGS) -c ./src/main.cpp $(INCLUDE) $(LIBDIR) $(LIBS)


WaldFtest.o : ./src/WaldFtest.cpp
	$(CXX) $(CFLAGS) -c ./src/WaldFtest.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

PBootstrap.o : ./src/PBootstrap.cpp
	$(CXX) $(CFLAGS) -c ./src/PBootstrap.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

LMESolve.o : ./src/LMESolve.cpp
	$(CXX) $(CFLAGS) -c ./src/LMESolve.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

RLRTsim.o : ./src/RLRTsim.cpp
	$(CXX) $(CFLAGS) -c ./src/RLRTsim.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

InputFile.o : ./src/InputFile.cpp
	$(CXX) $(CFLAGS) -c ./src/InputFile.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

DistanceRegression.o : ./src/DistanceRegression.cpp
	$(CXX) $(CFLAGS) -c ./src/DistanceRegression.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

RV.o : ./src/RV.cpp
	$(CXX) $(CFLAGS) -c ./src/RV.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

MathFun.o : ./src/MathFun.cpp
	$(CXX) $(CFLAGS) -c ./src/MathFun.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

clean:
	@echo $(OS)
	rm *.o
