//
//  simpleLogger.h
//  admmaXcode
//
//  Created by Emrah Kostem on 4/15/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//

#ifndef SIMPLELOGGER_H
#define SIMPLELOGGER_H

#include <string>
#include <sstream>
#include <iostream>

using namespace std;


class simpleLogger
{
public:
	simpleLogger( ostream dump );
	simpleLogger( string logfilename);
	~simpleLogger();
	template <class T>  void log( T t, ios_base & (*f)(ios_base&) );
private:
	
	template <class T> string to_string(T t, ios_base & (*f)(ios_base&));
	ostream dump;
	string logfilename;
	
};



template <class T>
inline string simpleLogger::to_string(T t, ios_base & (*f)(ios_base&))
{
	ostringstream oss;
	oss << f << t;
	return oss.str();
}


template <class T>
inline void simpleLogger::log(T t, ios_base & (*f)(ios_base&))
{
	dump << to_string( t, f);
}




simpleLogger::~simpleLogger()
{
	if (logfilename.size())
	{
		(fstream *)(&dump).close();
	}
}




#endif
