//
//  MathFun.h
//  admmaXcode
//
//  Created by Emrah Kostem on 4/22/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//

#ifndef MATHFUN_H
#define MATHFUN_H

#include "MKLconf.h"
#include "InputFile.h"
#include <iostream>
#include <string>
using namespace std;


const double ACCURACY = 1e-15;
const double EIGTHRSHLD = 1e-5;

/**********************************
 * Incomplete BETA function
 * I_x = BETA(x, a, b) / BETA(a,b)
 **********************************/
double betaIn(double x, double a, double b, string &errorMSG);


/**********************************
 * CDF of F-Distribution 
 * P(X <= x) 
 **********************************/
double cdfF(double x, int df1, int df2, string &errorMSG);

/**********************************
 * Computes the Projection Matrix of X
 * P = X (X'X)^-1 X'
 **********************************/
void getProjection(const double *X, int nrow, int ncol, double *P, string &errorMSG);


/**********************************
 * Computes the Projection Matrix of X
 * using generalized inverse
 * P = X (X'X)^- X'
 **********************************/
void getProjectionGinv(const double *X, int nrow, int ncol, double *P, string &errorMSG);


/**********************************
 * Computes the Trace of X
 **********************************/
int trace(const double *X, int n, string &errorMSG);

/**********************************
 * Computes the Sqrt of X
 **********************************/
void getSqrt(const double *X, int N, double *SQRT, string &errorMSG);

/**********************************
 * Concatenates X1 | X2 => C
 **********************************/
void concat(const double *X1, int nrow1, int ncol1, const double *X2, int nrow2, int ncol2, double *C, string &errorMSG);


/*
 * Read a matrix from file, learns nrow, ncol, allocates space.
 */
void readFile2Matrix(string filename, double *&matrix, int &nrow, int &ncol, string &errorMSG);


template<class T>
void printMatrix(T *M, int nrow, int ncol)
{
     for (int i=0; i < nrow; i++) {
        for (int j=0; j < ncol; j++) {
            cout << M[j*nrow + i] << " ";
        }
        cout << endl;
    }
    cout << endl;

}


#endif


