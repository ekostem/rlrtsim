// =====================================================================================
// 
//       Filename:  RLRTsim.h
// 
//    Description:  
// 
//        Version:  1.0
//        Created:  03/13/2011 17:20:12
//       Revision:  none
//       Compiler:  g++
// 
//         Author:  EMRAH KOSTEM (EK), kostem@gmail.com
//        Company:  www.emrahkostem.com
//      Copyright:  Copyright (c) 2011, EMRAH KOSTEM 
// =====================================================================================


#ifndef RLRTsim_H
#define RLRTsim_H

#include "MKLconf.h"
#include "MathFun.h"
#include <cstdlib>
#include <string>
#include <algorithm>
using namespace std;

class InputFile;
class RV;


class RLRTsim
{
public:
	RLRTsim(string FixedEffectFilename, string KinshipFilename, int nGrid, int seed, bool verbose); // read from a specific format
	RLRTsim(string FixedEffectFilename, string KinshipFilename, string ZFilename, int nGrid, int seed, bool verbose); // Takes into account Z mapping file
	~RLRTsim();
	
	void sample(double *stats, unsigned long num);
    double pvalue(string storeFilename, int num, double query);
	void storeSamples(double *stats, unsigned long num, string filename);
	
private:
	unsigned int sampleMultiplier;
    bool isVerbose;
	int seed; // for RV
	double *m_Pm, *m_Km, *m_Xm, *m_Zm; //matrices
	int m_nInd, m_nGrid; // num. individuals, num. fixed effect parameters
	int m_nK;
	int m_nXcol, m_nXrow;
	int m_nZcol, m_nZrow;
	
	double *m_ZspmValues;
	int *m_ZspRow, *m_ZspCol, m_nZnnz;
	
	double *ones;
	double *gridOnes;
	double *gridLambda; // grid of possible values for lambda
	double *gridEigenvalues;
	double *gridEigenvaluesDen;
	double *gridEigenvaluesDenInv;
	double *gridEigenvaluesLogTerm;
	double *logTerm;
	double *m_PmEigenValues;
    int m_nPmEigenValues;
	RV *rvGenerator;
	
    double *Fn_N, *Fn_D, *Fn_temp;
	void verboseMessage( string message );
	void verboseMessage( string message, int num);
	void verboseMessage( string message, double num);
	
	
	double Fn( double *samples );
	void getEigenvalues(const double *matrix, int n, double *eigenvalues);
	void sampleChiSq(double *samples, int num);
	void setm_Pm(void);
	void setGrid(void);
	void massageKinship(void);
	void sparseZ(void);

	
	
	
	
	
};


#endif
