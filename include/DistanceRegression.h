//
//  DistanceRegression.h
//  admmaXcode
//
//  Created by Emrah Kostem on 4/12/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//

#ifndef DISTANCEREGRESION_H
#define DISTANCEREGRESION_H

#include "MKLconf.h"
#include "RV.h"
#include <iostream>
using namespace std;

class RV;

class DistanceRegression
{
public:
	DistanceRegression(double *Kinship, int nrowKin, double *X, int ncolX);
	~DistanceRegression();
	
	double computePseudoF(double *G, double *H, double *I, int n) const;
	void samplePseudoF(double *G, double *H, double *I, int n, double *samples, int sampleSize);
	double getPvalue(double pseudoFvalue, double *samples, int sampleSize) const; 
	
private:
	double *m_Km, *m_Xm, *m_Gm, *m_Hm, *m_Im;
	int m_ncol, m_nrow;
	
	void getProjection(double *X, double *Y, int nrow, int ncol);
	void centralize(double *X, double *Y, int n);
	void permute(double *X, double *Y, int nrow, int ncol, int seed);
	double trace(double *X, int n) const;


};

#endif
