/*
 *  InputFile.h
 *  OFSS
 *
 *  Created by Emrah Kostem on 2/16/10.
 *  Copyright 2010 _EMRAH_KOSTEM_. All rights reserved.
 *
 */

#ifndef INPUTFILE_H
#define INPUTFILE_H

#pragma warning(disable:981)

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <stdexcept>

using namespace std;

class InputFilexception : public runtime_error
{
public:
	InputFilexception( const string& what) : runtime_error(what) {}
};


class InputFile
{
public:
	// Skips and splits
	InputFile (const string filename, const char* delimiter=NULL, const char* skip=NULL);
	~InputFile ();
	vector <string>* operator[](int i) const;
	vector <string>* at(int i) const;
	int length() {return lines_.size();};
	void print();

private:
	string filename_;
	vector <vector <string>*>::iterator file_begin;
	vector <vector <string>*>::iterator file_end;
	vector <vector <string>*>::iterator current_line;

	vector <string>::iterator line_begin;
	vector <string>::iterator line_end;
	vector <string>::iterator line_pos;
	vector <vector <string>*> lines_;
};


#endif
