//
//  RandomVariable.h
//  ADMMA
//
//  Created by Emrah Kostem on 3/13/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//

#ifndef RV_H
#define RV_H


#include "MKLconf.h"

class RV
{
public:
	RV(const int seed, const int brng=VSL_BRNG_MT19937);
	~RV() {vslDeleteStream( &stream_);}
	void rNormal(int count, double mean, double std, double* output, int method=VSL_METHOD_DGAUSSIAN_ICDF);
	void rMVNormal(int count, int dimension, const double* mean, const double* VarCovMatrix, double* output, int method=VSL_METHOD_DGAUSSIANMV_ICDF);
	void rUniform(int count, double left, double right, double* output, int method=VSL_METHOD_DUNIFORM_STD);
	
	
private:
	VSLStreamStatePtr stream_;
	int brng_;
	int seed_;
};




#endif
