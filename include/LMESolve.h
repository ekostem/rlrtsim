//
//  LMESolve.h
//  admmaXcode
//
//  Created by Emrah Kostem on 3/14/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//

#ifndef LMESOLVE_H
#define LMESOLVE_H

#include <iostream>
using namespace std;

class LMESolve
{
public:
	LMESolve(const double *Y, const double *X, const double* Kr, const double *Kg, int nY, int nBeta, int numGridPoints);
	~LMESolve();
	
	double logLikelihood(double varR, double varG, double varE, bool isNull) const;
	double gridSearch(bool isNull) const;
	void eigen(const double *Smatrix, int N, double *values, double *vectors) const;
	double detCholesky(const double* matrix, int dim) const;
	double detLU( const double* Smatrix, int dim) const;
	void solve(const double* A, double *X, const double* B, int n, int p) const;
	void printMatrix(double* matrix, int nrow, int ncol) const;
	
	void setY(double* newY);
	
private:
	double *m_Y, *m_X, *m_Beta, *m_Kr, *m_Kg;
	int m_numIndividuals, m_nBeta, m_nKr, m_nKg;
	double *m_gridVarE, *m_gridVarR, *m_gridVarG, *m_grid;
	int m_numGridPoints;
	
};





#endif

