//
//  PBootstrap.h
//  admmaXcode
//
//  Created by Emrah Kostem on 3/14/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//

#ifndef PBOOTSTRAP_H
#define PBOOTSTRAP_H

#include <string>
#include <algorithm>
#include <iostream>
using namespace std;

class RV;
class InputFile;
class LMESolve;


class PBootstrap
{
public:
	
	PBootstrap(string XFilename, string infoFilename, string KrFilename, string KgFilename, int sampleSize, int seed);
	~PBootstrap();
	
	bool readFile2Matrix(string filename, double *&matrix, int &n, int &m);
	void printMatrix(double* matrix, int nrow, int ncol) const;
	bool generateSamples(double *&samples, int &numSamples, int &numIndividuals) const; 
	void testLMESolve() const;
	
private:
	int m_numSamples, m_numIndividuals, m_nKr, m_nKg, m_nBeta;
	int m_seed;
	double *m_Kr, *m_Kg, *m_X, *m_Beta;
	RV *m_rvGenerator;	
	double m_varR, m_varG, m_varE;
	
	
	
};






#endif

