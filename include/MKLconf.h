//
//  configuration.h
//  ADMMA
//
//  Created by Emrah Kostem on 3/13/11.
//  Copyright 2011 Emrah Kostem. All rights reserved.
//


#ifndef MKLCONF_H
#define MKLCONF_H

#pragma warning (disable:981)

extern "C"
{
	// Get declaration for intel MKL
#include "math.h"
#include "mathimf.h"
#include "mkl.h"
#include "mkl_vml.h"
#include "mkl_vsl.h"
#include "mkl_vsl_functions.h"
#include "mkl_vml_functions.h"
#include "mkl_trans.h"
#include "mkl_blas.h"
#include "mkl_spblas.h"
#include "mkl_lapack.h"
}





#endif
