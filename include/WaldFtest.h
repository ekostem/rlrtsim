/*
 * =====================================================================================
 *
 *       Filename:  WaldFtest.h
 *
 *    Description:  Wald's Ftest for variance component.
 *
 *        Version:  1.0
 *        Created:  04/25/2011 13:14:03
 *       Revision:  none
 *       Compiler:  icpc
 *
 *         Author:  EMRAH KOSTEM (EK), kostem@gmail.com
 *        Company:  www.emrahkostem.com
 *
 * =====================================================================================
 */

#ifndef WALDFTEST_H
#define WALDFTEST_H

#include "MKLconf.h"
#include "MathFun.h"


class WaldTest 
{
public:
	WaldTest(string phenotypeFile, string covarianceFile, string regionKinFile, string genomicKinFile, double varGenomic, bool verbose);
	~WaldTest();
	
	double statistic(void);
	int df1(void);
	int df2(void);
	double pvalue(void);
	
	
	
private:
	bool isVerbose;
	int m_df1, m_df2;
	double m_statistic;
	double m_varGenomic;
	double *m_Ym, *m_Xm, *m_regionKm, *m_genomicKm;
	int m_nInd, m_nXrow, m_nXcol, m_nregionK, m_ngenomicK;
	
	void verboseMessage( string message );
	void verboseMessage( string message, int num);
	void verboseMessage( string message, double num);

};

#endif

